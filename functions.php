<?php
/**
 * towerpower Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package towerpower
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_TOWERPOWER_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'towerpower-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_TOWERPOWER_VERSION, 'all' );
    wp_enqueue_script( 'towerpower-theme-script', get_stylesheet_directory_uri() . '/js/functions.js', array( 'jquery' ), '1.0', true );

}
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

/**
 * ACF Options Page
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' => 'TowerPower Options'
    ));
}

/**
 * Shortcode: Schedule
 */
function tp_kursplan() {

    $page_kursplan = get_field('page_kursplan', 'option');
    $html = '';

    $monday = get_field('montag', get_the_ID());
    $monday_html = '';
    $monday_noon_html = '';

    $dienstag = get_field('dienstag', get_the_ID());
    $dienstag_html = '';
    $dienstag_noon_html = '';

    $mittwoch = get_field('mittwoch', get_the_ID());
    $mittwoch_html = '';
    $mittwoch_noon_html = '';

    $donnerstag = get_field('donnerstag', get_the_ID());
    $donnerstag_html = '';
    $donnerstag_noon_html = '';

    $freitag = get_field('freitag', get_the_ID());
    $freitag_html = '';
    $freitag_noon_html = '';

    $samstag = get_field('samstag', get_the_ID());
    $samstag_html = '';
    $samstag_noon_html = '';

    $sonntag = get_field('sonntag', get_the_ID());
    $sonntag_html = '';
    $sonntag_noon_html = '';

    if( have_rows('montag', get_the_ID()) ): 
        while( have_rows('montag', get_the_ID()) ): the_row(); 
            if( have_rows('kurs') ) :
                while( have_rows('kurs') ): the_row();
                    $logo = get_sub_field('logo');
                    $logo_size = 'medium';
                    $time_hr = explode(":", get_sub_field('uhrzeit_von'));
                    if ( intval($time_hr[0]) < 14 ) {
                        $monday_html .= '<div class="container-course">';
                        $monday_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $monday_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $monday_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $monday_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $monday_html .= '</div>';
                    } else {
                        $monday_noon_html .= '<div class="container-course">';
                        $monday_noon_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $monday_noon_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $monday_noon_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else :
                            $monday_noon_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $monday_noon_html .= '</div>';
                    }
                endwhile;
            endif;
        endwhile;
    endif;

    if( have_rows('dienstag', get_the_ID()) ): 
        while( have_rows('dienstag', get_the_ID()) ): the_row(); 
            if( have_rows('kurs') ) :
                while( have_rows('kurs') ): the_row();
                    $logo = get_sub_field('logo');
                    $logo_size = 'medium';
                    $time_hr = explode(":", get_sub_field('uhrzeit_von'));
                    if ( intval($time_hr[0]) < 14 ) {
                        $dienstag_html .= '<div class="container-course">';
                        $dienstag_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $dienstag_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $dienstag_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $dienstag_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $dienstag_html .= '</div>';
                    } else {
                        $dienstag_noon_html .= '<div class="container-course">';
                        $dienstag_noon_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $dienstag_noon_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $dienstag_noon_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $dienstag_noon_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $dienstag_noon_html .= '</div>';
                    }
                endwhile;
            endif;
        endwhile;
    endif;

    if( have_rows('mittwoch', get_the_ID()) ): 
        while( have_rows('mittwoch', get_the_ID()) ): the_row(); 
            if( have_rows('kurs') ) :
                while( have_rows('kurs') ): the_row();
                    $logo = get_sub_field('logo');
                    $logo_size = 'medium';
                    $time_hr = explode(":", get_sub_field('uhrzeit_von'));
                    if ( intval($time_hr[0]) < 14 ) {
                        $mittwoch_html .= '<div class="container-course">';
                        $mittwoch_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $mittwoch_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $mittwoch_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $mittwoch_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $mittwoch_html .= '</div>';
                    } else {
                        $mittwoch_noon_html .= '<div class="container-course">';
                        $mittwoch_noon_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $mittwoch_noon_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $mittwoch_noon_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $mittwoch_noon_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $mittwoch_noon_html .= '</div>';
                    }
                endwhile;
            endif;
        endwhile;
    endif;

    if( have_rows('donnerstag', get_the_ID()) ): 
        while( have_rows('donnerstag', get_the_ID()) ): the_row(); 
            if( have_rows('kurs') ) :
                while( have_rows('kurs') ): the_row();
                    $logo = get_sub_field('logo');
                    $logo_size = 'medium';
                    $time_hr = explode(":", get_sub_field('uhrzeit_von'));
                    if ( intval($time_hr[0]) < 14 ) {
                        $donnerstag_html .= '<div class="container-course">';
                        $donnerstag_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $donnerstag_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $donnerstag_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $donnerstag_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $donnerstag_html .= '</div>';
                    } else {
                        $donnerstag_noon_html .= '<div class="container-course">';
                        $donnerstag_noon_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $donnerstag_noon_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $donnerstag_noon_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $donnerstag_noon_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $donnerstag_noon_html .= '</div>';
                    }
                endwhile;
            endif;
        endwhile;
    endif;

    if( have_rows('freitag', get_the_ID()) ): 
        while( have_rows('freitag', get_the_ID()) ): the_row(); 
            if( have_rows('kurs') ) :
                while( have_rows('kurs') ): the_row();
                    $logo = get_sub_field('logo');
                    $logo_size = 'medium';
                    $time_hr = explode(":", get_sub_field('uhrzeit_von'));
                    if ( intval($time_hr[0]) < 14 ) {
                        $freitag_html .= '<div class="container-course">';
                        $freitag_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $freitag_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $freitag_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $freitag_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $freitag_html .= '</div>';
                    } else {
                        $freitag_noon_html .= '<div class="container-course">';
                        $freitag_noon_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $freitag_noon_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $freitag_noon_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $freitag_noon_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $freitag_noon_html .= '</div>';
                    }
                endwhile;
            endif;
        endwhile;
    endif;

    if( have_rows('samstag', get_the_ID()) ): 
        while( have_rows('samstag', get_the_ID()) ): the_row(); 
            if( have_rows('kurs') ) :
                while( have_rows('kurs') ): the_row();
                    $logo = get_sub_field('logo');
                    $logo_size = 'medium';
                    $time_hr = explode(":", get_sub_field('uhrzeit_von'));
                    if ( intval($time_hr[0]) < 14 ) {
                        $samstag_html .= '<div class="container-course">';
                        $samstag_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $samstag_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $samstag_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $samstag_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $samstag_html .= '</div>';
                    } else {
                        $samstag_noon_html .= '<div class="container-course">';
                        $samstag_noon_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        if (get_sub_field('raum')) : $samstag_noon_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                        if ($logo) :
                            $samstag_noon_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $samstag_noon_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $samstag_noon_html .= '</div>';
                    }
                endwhile;
            endif;
        endwhile;
    endif;

    if( have_rows('sonntag', get_the_ID()) ): 
        while( have_rows('sonntag', get_the_ID()) ): the_row(); 
            if( have_rows('kurs') ) :
                while( have_rows('kurs') ): the_row();
                    $logo = get_sub_field('logo');
                    $logo_size = 'medium';
                    $time_hr = explode(":", get_sub_field('uhrzeit_von'));
                    if ( intval($time_hr[0]) < 14 ) {
                        $sonntag_html .= '<div class="container-course">';
                        $sonntag_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        $sonntag_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>';
                        if ($logo) :
                            $sonntag_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $sonntag_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $sonntag_html .= '</div>';
                    } else {
                        $sonntag_noon_html .= '<div class="container-course">';
                        $sonntag_noon_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                        $sonntag_noon_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>';
                        if ($logo) :
                            $sonntag_noon_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                        else : 
                            $sonntag_noon_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                        endif;
                        $sonntag_noon_html .= '</div>';
                    }
                endwhile;
            endif;
        endwhile;
    endif;

    $html .= '<table class="table-course">';
    $html .= '    <thead>';
    $html .= '        <tr>';
    $html .= '            <th>Montag</th>';
    $html .= '            <th>Dienstag</th>';
    $html .= '            <th>Mittwoch</th>';
    $html .= '            <th>Donnerstag</th>';
    $html .= '            <th>Freitag</th>';
    $html .= '            <th>Samstag</th>';
    $html .= '            <th>Sonntag</th>';
    $html .= '        </tr>';
    $html .= '    </thead>';
    $html .= '    <tbody>';
    $html .= '        <tr>';
    $html .= '            <td>'.$monday_html.'</td>';
    $html .= '            <td>'.$dienstag_html.'</td>';
    $html .= '            <td>'.$mittwoch_html.'</td>';
    $html .= '            <td>'.$donnerstag_html.'</td>';
    $html .= '            <td>'.$freitag_html.'</td>';
    $html .= '            <td>'.$samstag_html.'</td>';
    $html .= '            <td>'.$sonntag_html.'</td>';
    $html .= '        </tr>';
    $html .= '        <tr>';
    $html .= '            <td>'.$monday_noon_html.'</td>';
    $html .= '            <td>'.$dienstag_noon_html.'</td>';
    $html .= '            <td>'.$mittwoch_noon_html.'</td>';
    $html .= '            <td>'.$donnerstag_noon_html.'</td>';
    $html .= '            <td>'.$freitag_noon_html.'</td>';
    $html .= '            <td>'.$samstag_noon_html.'</td>';
    $html .= '            <td>'.$sonntag_noon_html.'</td>';
    $html .= '        </tr>';
    $html .= '    </tbody>';
    $html .= '</table>';

    return $html;
}
add_shortcode( 'kursplan', 'tp_kursplan' );

/**
 * Shortcode: Schedule
 */
function tp_kursplan_widget() {
    $current_day    = date('l');
    $kursplan_id    = get_field('page_kursplan', 'option');
    $schedule_day   = '';
    $schedule_day_next = '';
    $html           = '';
    $schedule_html  = '';
    $schedule_next_html  = '';

    switch ($current_day) {
        case 'Monday':
        case 'Montag':
            $schedule_day = 'montag';
            $schedule_day_next = 'dienstag';
            break;

        case 'Tuesday':
        case 'Dienstag':
            $schedule_day = 'dienstag';
            $schedule_day_next = 'mittwoch';
            break;

        case 'Wednesday':
        case 'Mittwoch':
            $schedule_day = 'mittwoch';
            $schedule_day_next = 'donnerstag';
            break;

        case 'Thursday':
        case 'Donnerstag':
            $schedule_day = 'donnerstag';
            $schedule_day_next = 'freitag';
            break;

        case 'Friday':
        case 'Freitag':
            $schedule_day = 'freitag';
            $schedule_day_next = 'samstag';
            break;

        case 'Saturday':
        case 'Samstag':
            $schedule_day = 'samstag';
            $schedule_day_next = 'sonntag';
            break;

        case 'Sunday':
        case 'Sonntag':
            $schedule_day = 'sonntag';
            $schedule_day_next = 'montag';
            break;
    }


    $limit = 4;
    $count = 1;
    $no_course_today = false;

    if( have_rows($schedule_day, $kursplan_id) ): 
        while( have_rows($schedule_day, $kursplan_id) ): the_row(); 
            if( have_rows('kurs') ) :
                while( have_rows('kurs') ): the_row();
                    $logo = get_sub_field('logo');
                    $logo_size = 'medium';
                    $time_hr = explode(":", get_sub_field('uhrzeit_von'));
                    if ( intval($time_hr[0]) >= intval(current_time('G')) ) :
                        if ($count<=$limit) :
                            $schedule_html .= '<div class="container-course">';
                            $schedule_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                            if (get_sub_field('raum')) : $schedule_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                            if ($logo) :
                                $schedule_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                            else : 
                                $schedule_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                            endif;
                            $schedule_html .= '</div>';
                            $count++;
                        endif;
                    endif;
                endwhile;

                if ( $count == 1 ) : 
                    $no_course_today = true;
                endif;

                if ($no_course_today) :
                    $schedule_html .= '<div class="container-course">';
                    $schedule_html .=   '<em>Heute keine Kurse mehr …</em>';
                    $schedule_html .= '</div>';
                endif;
            endif;
        endwhile;
    endif;

    if ($count<$limit) :
        if( have_rows($schedule_day_next, $kursplan_id) ): 
            while( have_rows($schedule_day_next, $kursplan_id) ): the_row(); 
                if( have_rows('kurs') ) :
                    while( have_rows('kurs') ): the_row();
                        $logo = get_sub_field('logo');
                        $logo_size = 'medium';
                        if ( $count<$limit ) :
                            $schedule_next_html .= '<div class="container-course">';
                            $schedule_next_html .=     get_sub_field('uhrzeit_von') . ' – ' . get_sub_field('uhrzeit_bis') . ' Uhr ';
                            if (get_sub_field('raum')) : $schedule_next_html .=     '<span class="room-' . strtolower( get_sub_field('raum') ) . '">' . get_sub_field('raum') . '</span>'; endif;
                            if ($logo) :
                                $schedule_next_html .= wp_get_attachment_image( $logo, $logo_size, false, array('alt' => get_sub_field('kurs'), 'title' => get_sub_field('kurs')) );
                            else : 
                                $schedule_next_html .= '<br><strong>'.get_sub_field('kurs').'</strong>';
                            endif;
                            $schedule_next_html .= '</div>';
                            $count++;
                        endif;
                    endwhile;
                endif;
            endwhile;
        endif;
    endif;

    $html .= '<table class="table-course">';
    $html .= '    <thead>';
    $html .= '        <tr>';
    $html .= '            <th>Heute, ' . ucfirst($schedule_day) . '</th>';
    $html .= '        </tr>';
    $html .= '    </thead>';
    $html .= '    <tbody>';
    $html .= '        <tr>';
    $html .= '            <td>'.$schedule_html.'</td>';
    $html .= '        </tr>';
    $html .= '    </tbody>';
    $html .= '</table>';

    if (!empty($schedule_next_html)) :
        $html .= '<table class="table-course">';
        $html .= '    <thead>';
        $html .= '        <tr>';
        $html .= '            <th>Morgen, ' . ucfirst($schedule_day_next) . '</th>';
        $html .= '        </tr>';
        $html .= '    </thead>';
        $html .= '    <tbody>';
        $html .= '        <tr>';
        $html .= '            <td>'.$schedule_next_html.'</td>';
        $html .= '        </tr>';
        $html .= '    </tbody>';
        $html .= '</table>';
    endif;

    return $html;
}
add_shortcode( 'kursplan_widget', 'tp_kursplan_widget' );

/**
 * Helper
 */
function tp_debug( $var ) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}