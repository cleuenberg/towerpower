/* Custom JS Functions */
jQuery(document).ready(function($) {
    if ( $('.pp-member-wrapper').length ) {
        $('.pp-member-wrapper').hover(
            function() {
                var member_src = $(this).find('img.fl-photo-img').attr('src');
                console.log('member_src: ' + member_src);
                var member_srcset = $(this).find('img.fl-photo-img').attr('srcset');
                var member_src_over = member_src.replace("_01", "_02");
                var member_srcset_over = member_srcset.replace("_01", "_02");
                $(this).find('img.fl-photo-img').attr('src', member_src_over);
                $(this).find('img.fl-photo-img').attr('srcset', member_srcset_over);
            }, function() {
                var member_src = $(this).find('img.fl-photo-img').attr('src');
                var member_srcset = $(this).find('img.fl-photo-img').attr('srcset');
                var member_src_over = member_src.replace("_02", "_01");
                var member_srcset_over = member_srcset.replace("_02", "_01");
                $(this).find('img.fl-photo-img').attr('src', member_src_over);
                $(this).find('img.fl-photo-img').attr('srcset', member_srcset_over);
            }
        );
    }

    if ( $('.fl-pricing-table').length ) {
        $('.fl-pricing-table .fl-pricing-table-features li').each(function( index ) {
            if ( $(this).text() == 'Kurse' ) {
                $(this).css('border-left','8px solid #B4D8EB');
                $(this).css('margin-left','-8px');
                $(this).css('font-weight','bold');
            } else if ( $(this).text() == 'Fitness' ) {
                $(this).css('border-left','8px solid #BBC0EE');
                $(this).css('margin-left','-8px');
                $(this).css('font-weight','bold');
            } else if ( $(this).text() == 'Wellness' ) {
                $(this).css('border-left','8px solid #FFEEC0');
                $(this).css('margin-left','-8px');
                $(this).css('font-weight','bold');
            } else if ( $(this).text() == 'Reha' ) {
                $(this).css('border-left','8px solid #FFE2C0');
                $(this).css('margin-left','-8px');
                $(this).css('font-weight','bold');
            }
        });
    }
});